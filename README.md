# Web/Api Forum

## 💻 Pré-requisitos

Antes de começar, verifique se você atendeu aos seguintes requisitos:
* DOCKER


## 🚀 Instalando o projeto:
Para instalar, siga estas etapas:

Navegue até o diretório onde estão localizados seus projetos:
```
cd projects
```
Clone o repositório:
```
git clone https://gitlab.com/RomuloCVieira/curso-laravel-10.git
```
## 🔧 Construindo o projeto:
Navegue até o diretório do projeto:
```
cd curso-laravel-10
```
Crie arquivo .env
```
cp .env.example .env
```
Atualize essas variáveis de ambiente no arquivo .env
```dosini
APP_NAME="Laravel 10"
APP_URL=http://localhost

DB_CONNECTION=mysql
DB_HOST=db
DB_PORT=3306
DB_DATABASE=database
DB_USERNAME=user
DB_PASSWORD=password

CACHE_DRIVER=redis
QUEUE_CONNECTION=redis
SESSION_DRIVER=redis

REDIS_HOST=redis
REDIS_PASSWORD=null
REDIS_PORT=6379
```
Suba o container:
```
docker-compose up -d --build
```

Instale as dependências do projeto:
```
docker-compose exec app composer install
```
Gere a key do projeto Laravel:
```
docker-compose exec app php artisan key:generate
```
Gere as tabelas no banco Laravel:
```
docker-compose exec app php artisan migrate
```
Gere os usuarios do sistema:
```
docker-compose exec app php artisan db:seed
```
Lista de usuarios:
```
'email' => 'usuario1@email.com',
'password' => 'password'

'email' => 'usuario2@email.com',
'password' => 'password'
```

### Acesse o host:

http://localhost

# O que foi trabalhado no projeto?
#### Padrões de Projeto:
* Padrão Observer: Exploração e aplicação do padrão Observer para notificar e reagir a mudanças em objetos.
* Adapter: Utilização do padrão Adapter para conectar interfaces de classes diferentes.

#### Funcionalidades de Email e Mudança de Status:
* Fila com Redis para Envio de Email: Implementação de filas utilizando Redis para processar envios de email de forma assíncrona.
* Mudança de Status com Fila: Utilização de filas para processar mudanças de status.

#### Conceitos de Desenvolvimento:
* DTO (Data Transfer Object): Utilização de objetos para transferência de dados entre subsistemas.
* Repositórios e Serviços: Organização de camadas de dados e lógica de negócios através de repositórios e serviços.
* Presenter: Utilização de presenters para formatar e exibir dados em camadas de apresentação.
* Autorização e Autenticação: Implementação de sistemas de autorização e autenticação para segurança de acesso.

#### Eventos e Listeners:
* Events e Listeners: Utilização do conceito de eventos e ouvintes para reagir a eventos específicos na aplicação.

#### Tecnologias Utilizadas:
* PHP: Linguagem principal utilizada no desenvolvimento.
* Redis: Uso do Redis para a implementação de filas e outros recursos de armazenamento em cache.
* API: Desenvolvimento e integração de APIs para interação com outros sistemas.
* Tailwind e Blade: Uso e aplicação do Tailwind CSS e Blade (template engine) no front-end.
* Web: Desenvolvimento web utilizando Laravel, PHP e suas funcionalidades.
* MySQL: Utilização do banco de dados MySQL para armazenamento de dados.

## 🎁 Expressões de gratidão

* 🌟 Quero expressar minha imensa gratidão pela oportunidade de mergulhar no curso de Laravel. Foi uma jornada incrível, repleta de aprendizados valiosos e experiências enriquecedoras. Ao longo deste curso, pude aprofundar meus conhecimentos em um dos frameworks mais poderosos.

## 🤝 Colaboradores
Colabolador:
<table>
  <tr>
    <td align="center">
      <a href="https://gitlab.com/RomuloCVieira/">
        <img src="https://gitlab.com/uploads/-/system/user/avatar/12269576/avatar.png?width=400" width="100px;" alt="Foto do Iuri Silva no GitHub"/><br>
        <sub>
          <b>Romulo C Vieira</b>
        </sub>
      </a>
    </td>
  </tr>
</table>
