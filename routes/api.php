<?php

use App\Http\Controllers\Api\Auth\AuthController;
use App\Http\Controllers\Api\DoubtController;
use App\Http\Controllers\Api\ReplyDoubtController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;



Route::middleware('auth:sanctum')->group(function () {
    Route::apiResource('/doubt',  DoubtController::class);
    Route::get('/me', [AuthController::class, 'me']);
    Route::get('doubt/{doubt_id}/replies',  [ReplyDoubtController::class, 'index']);
    Route::post('doubt/{doubt_id}/replies',  [ReplyDoubtController::class, 'store']);
    Route::delete('doubt/{id}/replies',  [ReplyDoubtController::class, 'destroy']);
});
Route::post('/login', [AuthController::class, 'auth']);
Route::post('/logout', [AuthController::class, 'logout']);

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
