<?php

use App\Http\Controllers\Admin\DoubtController;
use App\Http\Controllers\Admin\ReplyDoubtController;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;


Route::get('/', function () {
    return view('auth.login');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');

    Route::post('/doubt/{id}/replies', [ReplyDoubtController::class, 'store'])->name('replies.store');
    Route::delete('/doubt/{id}/replies/{reply_id}', [ReplyDoubtController::class, 'destroy'])->name('replies.destroy');
    Route::get('/doubt/{id}/replies', [ReplyDoubtController::class, 'index'])->name('replies.index');

    Route::delete('/doubt/{id}', [DoubtController::class, 'destroy'])->name('doubt.destroy');
    Route::put('/doubt/{id}', [DoubtController::class, 'update'])->name('doubt.update');
    Route::get('/doubt/{id}/edit', [DoubtController::class, 'edit'])->name('doubt.edit');
    Route::get('/doubt/create', [DoubtController::class, 'create'])->name('doubt.create');
    Route::post('/doubt/store', [DoubtController::class, 'store'])->name('doubt.store');
    Route::get('/doubt', [DoubtController::class, 'index'])->name('doubt.index');
});

require __DIR__.'/auth.php';
