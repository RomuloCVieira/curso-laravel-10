<?php

namespace App\Models;

use App\Enums\DoubtStatus;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Doubt extends Model
{
    use HasFactory, HasUuids;

    protected $fillable = [
        'subject',
        'body',
        'status'
    ];

    protected function status(): Attribute
    {
        return Attribute::make(
            set: fn (DoubtStatus $status) => $status->name
        );
    }

    public function replies(): HasMany
    {
        return $this->hasMany(ReplyDoubt::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}

