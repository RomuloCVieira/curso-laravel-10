<?php

declare(strict_types=1);

namespace App\Enums;

use ValueError;

enum DoubtStatus: string
{
    case ACTIVE = 'Open';
    case PENDING = 'Pending';
    case CLOSED = 'Closed';

    public static function fromValue(string $value): string
    {
        foreach (self::cases() as $status) {
            if ($value === $status->name) {
                return $status->value;
            }
        }

        throw new ValueError("$value is not valid");
    }
}
