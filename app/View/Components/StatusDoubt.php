<?php

declare(strict_types=1);

namespace App\View\Components;

use Illuminate\View\Component;

class StatusDoubt extends Component
{

    public function __construct(
        protected string $status
    ) {}

    public function render()
    {
        switch ($this->status) {
            case 'CLOSED':
                $color = 'green';
                break;
            case 'PENDING':
                $color = 'red';
                break;
            default:
                $color = 'blue';
        }

        $textStatus = getStatusDoubt($this->status);
        return view('components.status-doubt', compact('textStatus', 'color'));
    }
}
