<?php

namespace App\Listeners;

use App\Events\DoubtReplied;
use App\Mail\DoubtRepliedMail;
use Illuminate\Support\Facades\Mail;

class SendMailWhenDoubtReplied
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(DoubtReplied $event): void
    {
         $reply = $event->getReply();

         Mail::to($reply->user['email'])->send(new DoubtRepliedMail($reply));
    }
}
