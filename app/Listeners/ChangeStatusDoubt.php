<?php

namespace App\Listeners;

use App\Enums\DoubtStatus;
use App\Events\DoubtReplied;
use App\Services\DoubtSevice;

class ChangeStatusDoubt
{
    /**
     * Create the event listener.
     */
    public function __construct(
        protected DoubtSevice $doubtSevice
    ) {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(DoubtReplied $event): void
    {
        $reply = $event->getReply();
        $this->doubtSevice->updateStatus(
            $reply->doubt_id,
            DoubtStatus::PENDING
        );
    }
}
