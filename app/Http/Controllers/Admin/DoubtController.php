<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\DTO\Doubts\CreateDoubtDTO;
use App\DTO\Doubts\UpdateDoubtDTO;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUpdateForumRequest;
use App\Services\DoubtSevice;
use Illuminate\Http\Request;


class DoubtController extends Controller
{
    public function __construct(
        protected DoubtSevice $doubtSevice
    ) {}

    public function index(Request $request)
    {
        $doubts = $this->doubtSevice->paginate(
            (int) $request->get('page', 1),
            (int) $request->get('per_page', 6),
            $request->filter,
        );

        $filters= ['filter' => $request->get('filter', '')];
        return view('admin.doubt.index',compact('doubts' , 'filters'));
    }

    public function create()
    {
        return view('admin.doubt.create');
    }

    public function store(StoreUpdateForumRequest $request)
    {
        $this->doubtSevice->new(
            CreateDoubtDTO::makeFromRequest($request)
        );

        return redirect()
            ->route('doubt.index')
            ->with('message', 'Cadastrado com sucesso!!!');
    }

    public function show(string $id)
    {
        $doubt = $this->doubtSevice->findOne($id);
        if (!$doubt) {
            return back();
        }

        return view('admin.doubt.show', compact('doubt'));
    }

    public function edit(string $id)
    {
        $doubt = $this->doubtSevice->findOne($id);
        if (!$doubt) {
            return back();
        }

        return view('admin.doubt.edit', compact('doubt'));
    }

    public function update(StoreUpdateForumRequest $request, string $id)
    {
        $doubt = $this->doubtSevice->update(
            UpdateDoubtDTO::makeFromRequest($request)
        );

        if (!$doubt) {
            return back();
        }

        return redirect()
                    ->route('doubt.index')
                    ->with('message', 'Atualizado com sucesso!!!');
    }

    public function destroy(string $id)
    {
        $this->doubtSevice->delete($id);

        return redirect()
            ->route('doubt.index')
            ->with('message', 'Deletado com sucesso!!!');
    }
}
