<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\DTO\Replies\CreateReplyDTO;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreReplyDoubtRequest;
use App\Services\DoubtSevice;
use App\Services\ReplyDoubtService;

class ReplyDoubtController extends Controller
{
    public function __construct(
        protected DoubtSevice $doubtSevice,
        protected ReplyDoubtService $replyDoubtService
    ) {}

    public function index(string $id)
    {
        $doubt = $this->doubtSevice->findOne($id);
        if (!$doubt) {
            return back();
        }

        $replies = $this->replyDoubtService->getAllByDoubtId($id);

        return view('admin.doubt.replies.replies', compact('doubt', 'replies'));
    }

    public function store(StoreReplyDoubtRequest $request)
    {
        $this->replyDoubtService->createNew(
            CreateReplyDTO::makeFromRequest($request)
        );

        return redirect()
            ->route('replies.index', $request->doubt_id)
            ->with('message', 'Cadastrado com sucesso!!!');
    }

    public function destroy(string $doubtId, string $replyId)
    {
        $this->replyDoubtService->delete($replyId);

        return redirect()
            ->route('replies.index', $doubtId)
            ->with('message', 'Deletado com sucesso!!!');
    }
}
