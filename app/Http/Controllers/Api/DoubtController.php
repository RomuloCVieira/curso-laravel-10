<?php

namespace App\Http\Controllers\Api;

use App\Adapters\ApiAdapter;
use App\DTO\Doubts\CreateDoubtDTO;
use App\DTO\Doubts\UpdateDoubtDTO;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUpdateForumRequest;
use App\Http\Resources\DoubtResource;
use App\Services\DoubtSevice;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class DoubtController extends Controller
{
    public function __construct(
        protected DoubtSevice $doubtSevice
    ) {}

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $pagination = $this->doubtSevice->paginate(
            (int) $request->get('page', 1),
            (int) $request->get('per_page', 1),
            $request->filter,
        );

        return ApiAdapter::toJson($pagination);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreUpdateForumRequest $request)
    {
        $doubt = $this->doubtSevice->new(
            CreateDoubtDTO::makeFromRequest($request)
        );

        return (new DoubtResource($doubt))->response()->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $doubt = $this->doubtSevice->findOne($id);

        if (!$doubt) {
            return response()->json(
                ['error' => 'Not found'],
                Response::HTTP_NOT_FOUND
            );
        }

        return new DoubtResource($doubt);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(StoreUpdateForumRequest $request, string $id)
    {
        $doubt = $this->doubtSevice->update(
            UpdateDoubtDTO::makeFromRequest($request, $id)
        );

        if (!$doubt) {
            return response()->json(
                ['error' => 'Not found'],
                Response::HTTP_NOT_FOUND
            );
        }

        return new DoubtResource($doubt);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $doubt = $this->doubtSevice->findOne($id);

        if (!$doubt) {
            return response()->json(
                ['error' => 'Not found'],
                Response::HTTP_NOT_FOUND
            );
        }

        $this->doubtSevice->delete($id);

        return response()->json([], Response::HTTP_NO_CONTENT);
    }
}
