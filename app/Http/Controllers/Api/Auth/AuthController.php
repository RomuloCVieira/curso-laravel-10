<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api\Auth;


use App\Http\Requests\Api\AuthRequest;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class AuthController
{
    public function auth(AuthRequest $request)
    {
        $credentials = $request->only(
            'email',
            'password',
            'device_name'
        );

        /** @var User $user */
        $user = User::where('email', $credentials['email'])->first();
        $isValidPassword = Hash::check($credentials['password'], $user->password);

        if (!$user || !$isValidPassword ) {
            throw ValidationException::withMessages([
                'email' => ['The provided credentials are incorrect']
            ]);
        }

        $token = $user->createToken($credentials['device_name']);

        return response()->json([
            'token' => $token->plainTextToken
        ]);
    }

    public function logout()
    {
        if (!$user = auth()->user()) {
            throw ValidationException::withMessages(['There is no active session for this user']);
        }

        $user->tokens()->delete();

        return response()->json([
            'message' => 'success'
        ]);
    }

    public function me()
    {
        $user = auth()->user();

        return response()->json([
            'user' => $user
        ]);
    }
}
