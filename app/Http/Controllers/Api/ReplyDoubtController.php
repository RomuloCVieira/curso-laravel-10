<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\DTO\Replies\CreateReplyDTO;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreReplyDoubtRequest;
use App\Http\Resources\ReplyDoubtResource;
use App\Services\DoubtSevice;
use App\Services\ReplyDoubtService;
use Illuminate\Http\Response;

class ReplyDoubtController extends Controller
{
    public function __construct(
        protected DoubtSevice $doubtSevice,
        protected ReplyDoubtService $replyDoubtService
    ) {}

    public function index(string $id)
    {
        $doubt = $this->doubtSevice->findOne($id);
        if (!$doubt) {
            response()->json(['message' => 'Not found'], Response::HTTP_NOT_FOUND);
        }

        $replies = $this->replyDoubtService->getAllByDoubtId($id);

        return ReplyDoubtResource::collection(
            $replies
        );
    }

    public function store(StoreReplyDoubtRequest $request)
    {
        $reply = $this->replyDoubtService->createNew(
            CreateReplyDTO::makeFromRequest($request)
        );

        return (new ReplyDoubtResource($reply))->response()->setStatusCode(Response::HTTP_CREATED);
    }

    public function destroy(string $id)
    {
        $this->replyDoubtService->delete($id);

        return response()->json([], Response::HTTP_NO_CONTENT);
    }
}
