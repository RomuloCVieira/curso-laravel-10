<?php

namespace App\Observers;

use App\Models\ReplyDoubt;
use Illuminate\Support\Facades\Auth;

class ReplyObserver
{
    public function creating(ReplyDoubt $doubt): void
    {
        if (Auth::user()) {
            $doubt->user_id = Auth::user()->id;
        }
    }
}
