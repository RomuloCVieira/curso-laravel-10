<?php

declare(strict_types=1);

namespace App\Observers;

use App\Models\Doubt;
use Illuminate\Support\Facades\Auth;

class DoubtObserver
{
    public function creating(Doubt $doubt): void
    {
        if (Auth::user()) {
            $doubt->user_id = Auth::user()->id;
        }
    }
}
