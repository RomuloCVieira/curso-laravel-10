<?php

namespace App\Providers;

use App\Models\Doubt;
use App\Models\ReplyDoubt;
use App\Observers\DoubtObserver;
use App\Observers\ReplyObserver;
use App\Repositories\Contracts\DoubtRepositoryInterface;
use App\Repositories\Contracts\ReplyRepositoryInterface;
use App\Repositories\DoubtEloquentORMRepository;
use App\Repositories\ReplyEloquentORMRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(
            DoubtRepositoryInterface::class,
            DoubtEloquentORMRepository::class
        );

        $this->app->bind(
            ReplyRepositoryInterface::class,
            ReplyEloquentORMRepository::class
        );
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Doubt::observe(DoubtObserver::class);
        ReplyDoubt::observe(ReplyObserver::class);
    }
}
