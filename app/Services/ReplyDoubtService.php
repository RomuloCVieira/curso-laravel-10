<?php

declare(strict_types=1);

namespace App\Services;

use App\DTO\Replies\CreateReplyDTO;
use App\Events\DoubtReplied;
use App\Repositories\Contracts\ReplyRepositoryInterface;
use stdClass;

class ReplyDoubtService
{
    public function __construct(
        protected ReplyRepositoryInterface $replyRepository
    ) {}

    public function getAllByDoubtId(string $doubtId): array
    {
        return $this->replyRepository->getAllByDoubtId($doubtId);
    }

    public function createNew(CreateReplyDTO $createReplyDTO): stdClass
    {
        $reply = $this->replyRepository->createNew(
            $createReplyDTO
        );

        DoubtReplied::dispatch($reply);

        return $reply;
    }

    public function delete(string $doubtId): bool
    {
        return $this->replyRepository->delete($doubtId);
    }
}
