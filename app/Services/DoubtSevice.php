<?php

declare(strict_types=1);

namespace App\Services;

use App\DTO\Doubts\CreateDoubtDTO;
use App\DTO\Doubts\UpdateDoubtDTO;
use App\Enums\DoubtStatus;
use App\Repositories\Contracts\DoubtRepositoryInterface;
use App\Repositories\Contracts\PaginationInterface;
use stdClass;

class DoubtSevice
{
    public function __construct(
      protected DoubtRepositoryInterface $doubtRepository
    ) {}

    public function getAll(string $filter = null): array
    {
        return $this->doubtRepository->getAll($filter);
    }

    public function findOne(string $id): stdClass|null
    {
        return $this->doubtRepository->findOne($id);
    }

    public function new(CreateDoubtDTO $createDoubtDTO): stdClass
    {
        return $this->doubtRepository->new($createDoubtDTO);
    }

    public function update(UpdateDoubtDTO $updateDoubtDTO): stdClass|null
    {
        return $this->doubtRepository->update($updateDoubtDTO);
    }

    public function delete(string $id): bool|null
    {
        return $this->doubtRepository->delete($id);
    }

    public function paginate(
        int $page = 1,
        int $totalPerPage = 15,
        string $filter = null
    ): PaginationInterface {
        return $this->doubtRepository->paginate($page, $totalPerPage, $filter);
    }

    public function updateStatus(string $id, DoubtStatus $status): void
    {
        $this->doubtRepository->updateStatus($id, $status);
    }
}
