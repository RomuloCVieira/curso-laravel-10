<?php

declare(strict_types=1);

namespace App\DTO\Doubts;

use App\Enums\DoubtStatus;
use App\Http\Requests\StoreUpdateForumRequest;

class CreateDoubtDTO
{
    public function __construct(
        public string $subject,
        public DoubtStatus $status,
        public string $body
    ) {}

    public static function makeFromRequest(StoreUpdateForumRequest $request): self
    {
        return new self(
            $request->subject,
            DoubtStatus::ACTIVE,
            $request->body
        );
    }
}
