<?php

declare(strict_types=1);

namespace App\DTO\Doubts;

use App\Enums\DoubtStatus;
use App\Http\Requests\StoreUpdateForumRequest;

class UpdateDoubtDTO extends CreateDoubtDTO
{
    public function __construct(
        public string $id,
        public string $subject,
        public DoubtStatus $status,
        public string $body,
    ) {
        parent::__construct($subject, $status, $body);
    }

    public static function makeFromRequest(StoreUpdateForumRequest $request, string $id = null): self
    {
        return new self(
            $id ?? $request->id,
            $request->subject,
            DoubtStatus::ACTIVE,
            $request->body
        );
    }
}
