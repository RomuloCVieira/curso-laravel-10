<?php

namespace App\DTO\Replies;

use App\Http\Requests\StoreReplyDoubtRequest;

class CreateReplyDTO
{
    public function __construct(
        public string $doubtId,
        public string $content
    ) {}

    public static function makeFromRequest(StoreReplyDoubtRequest $request): self
    {
        return new self(
            $request->doubt_id,
            $request->content
        );
    }
}
