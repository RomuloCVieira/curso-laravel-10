<?php

declare(strict_types=1);

namespace App\Repositories;

use App\DTO\Replies\CreateReplyDTO;
use App\Models\ReplyDoubt;
use App\Repositories\Contracts\ReplyRepositoryInterface;
use Illuminate\Support\Facades\Gate;
use stdClass;

class ReplyEloquentORMRepository implements ReplyRepositoryInterface
{
    public function __construct(
       protected ReplyDoubt $replyDoubt
    ) {}

    public function getAllByDoubtId(string $doubtId): array
    {
        $replies = $this->replyDoubt
            ->with(['user', 'doubt'])
            ->where('doubt_id', $doubtId)->get();
        return $replies->toArray();
    }

    public function createNew(CreateReplyDTO $createReplyDTO): stdClass
    {
        $reply = $this->replyDoubt->create([
            'content' => $createReplyDTO->content,
            'doubt_id' => $createReplyDTO->doubtId
        ]);

        $reply = $this->replyDoubt->find($reply->id);

        return (object) $reply->toArray();
    }

    public function delete(string $doubtId): bool
    {
        $reply = $this->replyDoubt->find($doubtId);

        if (Gate::denies('owner',$reply->user->id)) {
            abort(403, 'Not Authorized');
        }

        return (bool) $reply->delete();
    }
}
