<?php

namespace App\Repositories;

use App\DTO\Doubts\CreateDoubtDTO;
use App\DTO\Doubts\UpdateDoubtDTO;
use App\Enums\DoubtStatus;
use App\Models\Doubt;
use App\Repositories\Contracts\DoubtRepositoryInterface;
use App\Repositories\Contracts\PaginationInterface;
use Illuminate\Support\Facades\Gate;
use stdClass;

class DoubtEloquentORMRepository implements DoubtRepositoryInterface
{
    public function __construct(
        protected Doubt $doubt
    ) {}

    public function getAll(string $filter = null): array
    {
        return $this->doubt
                    ->where(function ($query) use ($filter) {
                        if ($filter) {
                            $query->where('subject', $filter);
                            $query->orWhere('body', 'like', "%{$filter}%");
                        }
                     })
                    ->get()
                    ->toArray();
    }

    public function findOne(string $id): stdClass|null
    {
        $doubt = $this->doubt->with('user')->find($id);

        if (!$doubt) {
            return $doubt;
        }

        return (object) $doubt->toArray();
    }

    public function delete(string $id): bool|null
    {
        $doubt = $this->doubt->findOrFail($id);

        if (Gate::denies('owner', $doubt->user->id)) {
            abort(403, 'Not Authorized');
        }

        return $this->doubt->findOrFail($id)->delete();
    }

    public function new(CreateDoubtDTO $createDoubtDTO): stdClass
    {
        $doubt = (object) $this->doubt->create((array) $createDoubtDTO);

        return (object) $doubt->toArray();
    }

    public function update(UpdateDoubtDTO $updateDoubtDTO): stdClass|null
    {
        $doubt = $this->doubt->find($updateDoubtDTO->id);

        if (!$doubt) {
            return null;
        }

        if (Gate::denies('owner', $doubt->user->id)) {
            abort(403, 'Not Authorized');
        }

        $doubt->update(
            (array) $updateDoubtDTO
        );

        return (object) $doubt->toArray();
    }

    public function paginate(int $page = 1, int $totalPerPage = 15, string $filter = null): PaginationInterface
    {
        $result = $this->doubt
            ->with(['replies.user'])
            ->where(function ($query) use ($filter) {
                if ($filter) {
                    $query->where('subject', $filter);
                    $query->orWhere('body', 'like', "%{$filter}%");
                }
            })
            ->paginate($totalPerPage, ["*"], 'page', $page);

        return new PaginationPresenter($result);
    }

    public function updateStatus(string $id, DoubtStatus $status): void
    {
        $this->doubt->where('id', $id)->update([
            'status' => $status->name
        ]);
    }
}
