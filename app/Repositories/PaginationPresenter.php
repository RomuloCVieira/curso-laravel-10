<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Repositories\Contracts\PaginationInterface;
use Illuminate\Pagination\LengthAwarePaginator;
use stdClass;

class PaginationPresenter implements PaginationInterface
{
    /**
     * @var stdClass[]
     */
    private array $items;

    public function __construct(
        protected LengthAwarePaginator $lengthAwarePaginator
    ) {
        $this->items = $this->resolveItems($this->lengthAwarePaginator->items());
    }

    public function items(): array
    {
        return $this->items;
    }

    public function total(): int
    {
        return $this->lengthAwarePaginator->total() ?? 0;
    }

    public function isFirstPage(): bool
    {
        return $this->lengthAwarePaginator->onFirstPage();
    }

    public function isLastPage(): bool
    {
        return $this->lengthAwarePaginator->currentPage() == $this->lengthAwarePaginator->onLastPage();
    }

    public function currentPage(): int
    {
        return $this->lengthAwarePaginator->currentPage() ?? 1;
    }

    public function getNumberNextPage(): int
    {
        return $this->lengthAwarePaginator->currentPage() + 1;
    }

    public function getNumberPreviousPage(): int
    {
        return $this->lengthAwarePaginator->currentPage() - 1;
    }

    /**
     * @param array
     * @return stdClass[]
     */
    public function resolveItems(array $items): array
    {
        $result = [];
        foreach ($items as $item) {
            $stdClassObject = new stdClass();

            foreach ($item->toArray() as $key  => $value) {
                $stdClassObject->{$key} = $value;
            }

            array_push($result, $stdClassObject);
        }

        return $result;
    }
}
