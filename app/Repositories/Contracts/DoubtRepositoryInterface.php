<?php

declare(strict_types=1);

namespace App\Repositories\Contracts;

use App\Enums\DoubtStatus;
use App\DTO\{Doubts\CreateDoubtDTO, Doubts\UpdateDoubtDTO};
use stdClass;

interface DoubtRepositoryInterface
{
    public function getAll(string $filter = null): array;
    public function findOne(string $id): stdClass|null;
    public function delete(string $id): bool|null;
    public function new(CreateDoubtDTO $createDoubtDTO): stdClass;
    public function update(UpdateDoubtDTO $updateDoubtDTO): stdClass|null;
    public function paginate(int $page = 1, int $totalPerPage = 15, string $filter = null): PaginationInterface;
}
