<?php

namespace App\Repositories\Contracts;

use App\DTO\Replies\CreateReplyDTO;
use stdClass;

interface ReplyRepositoryInterface
{
    public function getAllByDoubtId(string $doubtId): array;
    public function createNew(CreateReplyDTO $createReplyDTO): stdClass;
    public function delete(string $doubtId): bool;
}
