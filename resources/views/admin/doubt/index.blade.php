@extends('admin.layouts.app')

@section('title', 'Doubts')

@section('header')
    @include('admin.doubt.partials.header', compact('doubts'))
@endsection

@section('content')
    @include('admin.doubt.partials.content')

    <x-pagination
        :paginator="$doubts"
        :appends="$filters" />
@endsection
