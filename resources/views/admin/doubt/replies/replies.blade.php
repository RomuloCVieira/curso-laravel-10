@extends('admin.layouts.app')

@section('title', "Detalhes da Dúvida {$doubt->subject}")

@section('content')
    <x-alert/>
    <div class="flex justify-center min-h-screen">
        <div class="md:w-3/5 w-3/4 px-10 flex flex-col gap-2 p-5 bg-white rounded-lg shadow-md">
            <div class="flex justify-between">
                <h1 class="text-lg">Detalhes da Dúvida <b>{{ $doubt->subject }}</b></h1>
                @can('owner', $doubt->user['id'])
                    <form action="{{ route('doubt.destroy', $doubt->id) }}" method="post">
                        @csrf()
                        @method('DELETE')
                        <button type="submit" class="bg-red-500 hover:bg-red-400 text-white font-bold py-2 px-4 border-b-4 border-red-700 hover:border-red-500 rounded">Deletar</button>
                    </form>
                @endcan
            </div>
            <ul class="mt-4">
                <li>Status: <x-status-doubt :status="$doubt->status"/></li>
                <li>Descrição: {{ $doubt->body }}</li>
            </ul>
            <div class="flex flex-col gap-3 mt-6">
                @forelse ($replies as $reply)
                    <div class="flex flex-col gap-4 bg-gray-100 rounded p-4">
                        <!-- Profile and Rating -->
                        <div class="flex justify-between items-center">
                            <div class="flex gap-2 items-center">
                                <div class="w-7 h-7 flex justify-center items-center rounded-full bg-red-500 text-white">{{getInitials($reply['user']['name'])}}</div>
                                <span>{{ $reply['user']['name'] }}</span>
                            </div>
                        </div>

                        <div>
                            {{ $reply['content'] }}
                        </div>

                        <div class="flex justify-between items-center">
                            <span>{{ $reply['created_at'] }}</span>
                            @can('owner', $reply['user']['id'])
                                <form action="{{ route('replies.destroy', [$doubt->id, $reply['id']]) }}" method="post">
                                    @csrf()
                                    @method('DELETE')
                                    <button type="submit" class="bg-red-500 hover:bg-red-400 text-white py-1 px-4 border-b-4 border-red-700 hover:border-red-500 rounded">Deletar</button>
                                </form>
                            @else
                                --
                            @endcan
                        </div>
                    </div>
                @empty
                    <p>No replies</p>
                @endforelse

                <div class="py-4">
                    <form action="{{ route('replies.store', $doubt->id) }}" method="post">
                        @csrf
                        <input type="hidden" name="doubt_id" value="{{ $doubt->id }}">
                        <textarea
                            rows="2"
                            name="content"
                            placeholder="Sua resposta"
                            class="w-full resize-none rounded-md border border-gray-300 bg-white py-3 px-6 text-base font-medium text-gray-700 outline-none focus:border-blue-500 focus:shadow-md"></textarea>
                        <button type="submit" class="hover:shadow-form rounded-md bg-blue-500 py-3 px-8 text-base font-semibold text-white outline-none mt-2">
                            Enviar
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
