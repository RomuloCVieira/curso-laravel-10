@extends('admin.layouts.app')

@section('title', "Editar a Dúvida {$doubt->subject}")

@section('header')
    <h1 class="text-lg text-black-500">Dúvida {{ $doubt->subject }}</h1>
@endsection

@section('content')
    <form action="{{ route('doubt.update', $doubt->id) }}" method="POST">
        @method('PUT')
        @include('admin.doubt.partials.form', [
            'support' => $doubt
        ])
    </form>
@endsection

