<?php

namespace Database\Seeders;

use App\Enums\DoubtStatus;
use App\Models\Doubt;
use App\Models\User;
use Faker\Factory;
use Illuminate\Database\Seeder;

class DoubtSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $faker = Factory::create();

        $users = User::all();

        foreach (range(1, 10) as $index) {
            $randomUser = $users->random();

            Doubt::create([
                'id' => $faker->uuid,
                'user_id' => $randomUser->id,
                'subject' => $faker->sentence(4),
                'status' => DoubtStatus::ACTIVE,
                'body' => $faker->text(50),
                'created_at' => $faker->dateTimeBetween('-1 year', 'now'),
                'updated_at' => $faker->dateTimeBetween('-1 year', 'now'),
            ]);
        }
    }
}
