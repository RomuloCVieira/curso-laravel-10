<?php

namespace Database\Seeders;

use App\Models\Doubt;
use App\Models\ReplyDoubt;
use App\Models\User;
use Faker\Factory;
use Illuminate\Database\Seeder;

class ReplySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $faker = Factory::create();
        $users = User::all();
        $doubts = Doubt::all();

        foreach (range(1, 20) as $index) {
            $randomUser = $users->random();
            $randomDoubt = $doubts->random();

            ReplyDoubt::create([
                'id' => $faker->uuid,
                'user_id' => $randomUser->id,
                'doubt_id' => $randomDoubt->id,
                'content' => $faker->text(50),
                'created_at' => $faker->dateTimeBetween('-1 year', 'now'),
                'updated_at' => $faker->dateTimeBetween('-1 year', 'now'),
            ]);
        }
    }
}
