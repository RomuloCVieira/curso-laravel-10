<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::factory()->create([
            'name' => 'Primeiro Usuário',
            'email' => 'usuario1@email.com',
            'password' => Hash::make('password')
        ]);

        User::factory()->create([
            'name' => 'Segundo Usuário',
            'email' => 'usuario2@email.com',
            'password' => Hash::make('password'),
        ]);

        User::factory()->create([
            'name' => 'Terceiro Usuário',
            'email' => 'usuario3@email.com',
            'password' => Hash::make('password'),
        ]);
    }
}
